package com.ulisses.booksonthetable.domain.formatters

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun String.toDate(): Date? {
    with(this) {
        return try {
            SimpleDateFormat("dd/MM/yyyy").parse(this)
        }catch (e: Exception) {
            null
        }
    }
}


@SuppressLint("SimpleDateFormat")
fun Date.toFormatString(): String {
    with(this) {
        val dateFormat = SimpleDateFormat("dd/mm/yyyy")
        return dateFormat.format(this)
    }

}