package com.ulisses.booksonthetable.ui.save

import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.lifecycle.Observer

import com.ulisses.booksonthetable.R
import com.ulisses.booksonthetable.domain.models.Book
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import com.ulisses.booksonthetable.ui.utils.MaskUtil
import com.ulisses.booksonthetable.viewmodels.SaveBookViewModel
import kotlinx.android.synthetic.main.activity_save_book.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SaveBookActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private val viewModel: SaveBookViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_save_book)

        configureEdits()
        configureSpinners()
        initObservables()

        edit_date_begin.text.toString()

        val extras = intent.getSerializableExtra("book") as Book?

        extras?.let { editBook ->
            editSetValueSpin(editBook.readStatus)
            edit_title.setText(editBook.title)
            edit_author.setText(editBook.authorName)
            edit_date_begin.setText(editBook.dateBegin)
            edit_date_end.setText(editBook.dateEnd)
        }

        btn_save_book.setOnClickListener {
            val book = Book(
                id = extras?.id ?: 0,
                title = edit_title.text.toString(),
                authorName = edit_author.text.toString(),
                genre = spin_genre.selectedItem.toString(),
                readStatus = spin_status.selectedItem.toString(),
                dateBegin = edit_date_begin.text.toString(),
                dateEnd = edit_date_end.text.toString()
            )
            viewModel.save(book)
        }

    }

    private fun initObservables() {
        viewModel.handleSuccess().observe(this, Observer {
            finish()
        })
        viewModel.handleErrorTitle().observe(this, Observer {
            makeAlert(getString(it))
        })

        viewModel.handleErrorAuthorName().observe(this, Observer {
            makeAlert(getString(it))
        })

        viewModel.handleErrorGenre().observe(this, Observer {
            makeAlert(getString(it))
        })

        viewModel.handleErrorReadStatus().observe(this, Observer {
            makeAlert(getString(it))
        })

        viewModel.handleErrorDateBegin().observe(this, Observer {
            makeAlert(getString(it))
        })

        viewModel.handleErrorDateEnd().observe(this, Observer {
            makeAlert(getString(it))
        })

        viewModel.handleShowDateBegin().observe(this, Observer { visible ->
            handleFieldsDate(textView5, edit_date_begin, visible)
        })

        viewModel.handleShowDateEnd().observe(this, Observer { visible ->
            handleFieldsDate(textView6, edit_date_end, visible)
        })
    }

    private fun handleFieldsDate(textView: TextView, editText: EditText, visible: Boolean) {
        if (visible) {
            textView.visibility = View.VISIBLE
            editText.visibility = View.VISIBLE
        } else {
            textView.visibility = View.GONE
            editText.visibility = View.GONE
            editText.setText("")
        }
    }

    private fun configureSpinners() {
        spin_status.onItemSelectedListener = this
    }

    private fun configureEdits() {
        edit_date_begin.addTextChangedListener(MaskUtil.mask(edit_date_begin, MaskUtil.FORMAT_DATA))
        edit_date_end.addTextChangedListener(MaskUtil.mask(edit_date_end, MaskUtil.FORMAT_DATA))
    }

    private fun editSetValueSpin(status: String) {
        when(status) {
            ReadStatus.READING.status -> {
                spin_status.setSelection(1)
            }
            ReadStatus.TO_READ.status -> {
                spin_status.setSelection(2)
            }
            ReadStatus.ALREADY_READ.status -> {
                spin_status.setSelection(3)
            }
        }
    }

    private fun makeAlert(message: String) {
        val builder = AlertDialog.Builder(this)
        builder
            .setTitle("Erro")
            .setMessage(message)
            .setPositiveButton("Sim") { dialog, _ ->
                dialog.dismiss()
            }
        builder.show()
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.showDate(parent?.getItemAtPosition(position).toString())
    }


}