package com.ulisses.booksonthetable.core.database.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import com.ulisses.booksonthetable.domain.formatters.toDate
import com.ulisses.booksonthetable.domain.formatters.toFormatString
import com.ulisses.booksonthetable.domain.models.Book
import java.util.*

@Entity
data class BookEntity(
        @PrimaryKey(autoGenerate = true) val id: Long = 0,
        val title: String,
        val authorName: String,
        val genre: String,
        val readStatus: ReadStatus,
        val read: Boolean = false,
        val dateBegin: Date? =  null,
        val dateEnd: Date? =  null
)

fun Book.toBookEntity(): BookEntity {
    return with(this) {
        BookEntity(
                id = this.id,
                title = this.title,
                authorName = this.authorName,
                genre = this.genre,
                readStatus = ReadStatus.fromString(this.readStatus) ?: ReadStatus.READING,
                read = this.read,
                dateBegin = this.dateBegin.toDate(),
                dateEnd = this.dateEnd.toDate()
        )
    }
}

fun BookEntity.toBook(): Book {
    return with(this) {
        Book(
                id = this.id,
                title = this.title,
                authorName = this.authorName,
                genre = this.genre,
                readStatus = this.readStatus.status,
                read = this.read,
                dateBegin = this.dateBegin?.toFormatString() ?: "",
                dateEnd = this.dateEnd?.toFormatString() ?: ""
        )
    }
}