package com.ulisses.booksonthetable.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ulisses.booksonthetable.R
import com.ulisses.booksonthetable.core.bases.BaseViewModel
import com.ulisses.booksonthetable.domain.formatters.toDate
import com.ulisses.booksonthetable.domain.interfaces.BookRepository
import com.ulisses.booksonthetable.domain.models.Book
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import kotlinx.coroutines.launch

class SaveBookViewModel(private val repository: BookRepository) : BaseViewModel() {

    private val _successSave = MutableLiveData<Unit>()
    private val _errorTitle = MutableLiveData<Int>()
    private val _errorAuthorName = MutableLiveData<Int>()
    private val _errorGenre = MutableLiveData<Int>()
    private val _errorReadStatus = MutableLiveData<Int>()
    private val _errorDateBegin = MutableLiveData<Int>()
    private val _errorDateEnd = MutableLiveData<Int>()

    private val _showDateBegin = MutableLiveData<Boolean>()
    private val _showDateEnd = MutableLiveData<Boolean>()

    fun handleSuccess(): LiveData<Unit> = _successSave
    fun handleErrorTitle(): LiveData<Int> = _errorTitle
    fun handleErrorAuthorName(): LiveData<Int> = _errorAuthorName
    fun handleErrorGenre(): LiveData<Int> = _errorGenre
    fun handleErrorReadStatus(): LiveData<Int> = _errorReadStatus
    fun handleErrorDateBegin(): LiveData<Int> = _errorDateBegin
    fun handleErrorDateEnd(): LiveData<Int> = _errorDateEnd
    fun handleShowDateBegin(): LiveData<Boolean> = _showDateBegin
    fun handleShowDateEnd(): LiveData<Boolean> = _showDateEnd

    private lateinit var book: Book

    fun save(book: Book) {

        this.book = book

        if (validBook()) {
            launch {
                repository.createBook(book)
                _successSave.value = Unit
            }
        }
    }

    private fun validBook(): Boolean {

        if (book.title.isEmpty()) {
            _errorTitle.value = R.string.error_type_title
            return false
        }

        if (book.authorName.isEmpty()) {
            _errorAuthorName.value = R.string.error_type_author_name
            return false
        }

        if (book.genre.equals("SELECIONE O GENERO")) {
            _errorGenre.value = R.string.error_type_genre
            return false
        }

        if (book.readStatus.equals("SELECIONE O STATUS")) {
            _errorReadStatus.value = R.string.error_type_read_status
            return false
        }

        return validDatesByStatus()
    }

    private fun validDatesByStatus(): Boolean {

        if(book.readStatus == ReadStatus.TO_READ.status) {
            if (book.dateEnd.isNotEmpty() || book.dateBegin.isNotEmpty()) {
                return false
            }
        }

        if (book.readStatus == ReadStatus.READING.status) {
            val dateBegin = book.dateBegin.toDate()

            if (dateBegin == null) {
                _errorDateBegin.value = R.string.error_type_date_begin
                return false
            }

        }

        if (book.readStatus == ReadStatus.ALREADY_READ.status) {
            val dateBegin = book.dateBegin.toDate()
            val dateEnd = book.dateEnd.toDate()

            if (dateBegin == null) {
                _errorDateBegin.value = R.string.error_type_date_begin
                return false
            }

            if (dateEnd == null) {
                _errorDateEnd.value = R.string.error_type_date_end
                return false
            }

        }

        return true
    }

    fun showDate(selectedValue: String) {
        if (selectedValue == ReadStatus.READING.status) {
            _showDateBegin.value = true
            if (_showDateEnd.value == true) {
                _showDateEnd.value = false
            }
            return
        }
        if (selectedValue == ReadStatus.ALREADY_READ.status) {
            _showDateEnd.value = true
            _showDateBegin.value = true
            return
        }
        _showDateEnd.value = false
        _showDateBegin.value = false
    }

}