package com.ulisses.booksonthetable.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ulisses.booksonthetable.R
import com.ulisses.booksonthetable.domain.models.Category
import kotlinx.android.synthetic.main.item_book_list_vertical.view.*

class BookListVerticalAdapter(private val categories: List<Category>, private val callback: BookListHorizontalAdapter.ClickListener): RecyclerView.Adapter<BookListVerticalAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BookListVerticalAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_book_list_vertical, parent, false))
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: BookListVerticalAdapter.ViewHolder, position: Int) {
        holder.bind(categories[position])
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        fun bind(category: Category){
            with(itemView) {
                itemView.txt_title.text = category.title
                val books = category.books

                this.rv_horizontal.layoutManager = LinearLayoutManager(
                    itemView.context, RecyclerView.HORIZONTAL, false
                )
                this.rv_horizontal.adapter = BookListHorizontalAdapter(books, callback)

            }
        }
    }

}