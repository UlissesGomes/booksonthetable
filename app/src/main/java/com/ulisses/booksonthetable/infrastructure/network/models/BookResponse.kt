package com.ulisses.booksonthetable.infrastructure.network.models

data class BookResponse(
    val title: String
)