package com.ulisses.booksonthetable.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ulisses.booksonthetable.R
import com.ulisses.booksonthetable.domain.models.Book
import kotlinx.android.synthetic.main.item_book_list.view.*

class BookListHorizontalAdapter(private val books: List<Book>, private val callback: ClickListener): RecyclerView.Adapter<BookListHorizontalAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BookListHorizontalAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_book_list, parent, false))
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: BookListHorizontalAdapter.ViewHolder, position: Int) {
        holder.bind(books[position])
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(book: Book) {
            itemView.title_book.text = book.title
            itemView.setOnClickListener {
                callback.onClick(book.id)
            }
        }
    }

    interface ClickListener {
        fun onClick(id: Long)
    }

}