package com.ulisses.booksonthetable.viewmodels

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ulisses.booksonthetable.core.bases.BaseViewModel
import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.domain.interfaces.BookRepository
import com.ulisses.booksonthetable.domain.models.Book
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import kotlinx.android.synthetic.main.activity_detail_book.*
import kotlinx.coroutines.launch

class DetailBookViewModel(private val repository: BookRepository): BaseViewModel() {

    private val _book = MutableLiveData<ResourceState<Book>>()
    private val _deleteBook = MutableLiveData<Boolean>()
    private val _hideDateBegin = MutableLiveData<Boolean>()
    private val _hideDateEnd = MutableLiveData<Boolean>()
    private val _hideButtonRead = MutableLiveData<Boolean>()

    fun handleBook(): LiveData<ResourceState<Book>> = _book
    fun handleDeleteBook(): LiveData<Boolean> = _deleteBook
    fun handleHideDateBegin(): LiveData<Boolean> = _hideDateBegin
    fun handleHideDateEnd(): LiveData<Boolean> = _hideDateEnd
    fun handleHideButtonRead(): LiveData<Boolean> = _hideButtonRead

    fun getBook(id: Long){
        launch {
            val book = repository.getBook(id)
            _book.value = ResourceState.Success(book)
            handleDates(book)
            handleButton(book.readStatus)
        }
    }

    private fun handleDates(book: Book){
        _hideDateBegin.value = book.dateBegin.isEmpty()
        _hideDateEnd.value = book.dateEnd.isEmpty()
    }

    private fun handleButton(status: String) {
        _hideButtonRead.value = ReadStatus.ALREADY_READ.status == status
    }

    fun delete(book: Book) {
        launch {
            repository.deleteBook(book)
            _deleteBook.value = true
        }
    }

}