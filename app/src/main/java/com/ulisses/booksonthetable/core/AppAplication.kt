package com.ulisses.booksonthetable.core

import android.app.Application
import com.ulisses.booksonthetable.core.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AppAplication: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@AppAplication)
            modules(databaseModule, viewModelModule, repositoryModule, netModule, apiModule)
        }
    }
}