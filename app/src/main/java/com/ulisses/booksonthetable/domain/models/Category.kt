package com.ulisses.booksonthetable.domain.models

data class Category(
    val title: String,
    val books: List<Book>
)
