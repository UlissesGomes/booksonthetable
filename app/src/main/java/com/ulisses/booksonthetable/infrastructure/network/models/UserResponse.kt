package com.ulisses.booksonthetable.infrastructure.network.models

data class UserResponse (
    val fullName: String,
    val email: String
)