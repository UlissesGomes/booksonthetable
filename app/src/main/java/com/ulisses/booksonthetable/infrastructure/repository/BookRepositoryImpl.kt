package com.ulisses.booksonthetable.infrastructure.repository

import com.ulisses.booksonthetable.core.database.dao.BookDao
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import com.ulisses.booksonthetable.core.database.entities.toBook
import com.ulisses.booksonthetable.core.database.entities.toBookEntity
import com.ulisses.booksonthetable.domain.interfaces.BookRepository
import com.ulisses.booksonthetable.domain.models.Book
import com.ulisses.booksonthetable.infrastructure.network.interfaces.BookApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BookRepositoryImpl(private val bookDao: BookDao, private val bookApi: BookApi) : BookRepository {

    override suspend fun createBook(book: Book) {
         bookDao.save(book.toBookEntity())
    }

    override suspend fun deleteBook(book: Book) {
        bookDao.delete(book.toBookEntity())
    }

    override suspend fun getBook(id: Long): Book {
        return bookDao.getBook(id).toBook()
    }

    override suspend fun getBookByStatus(readStatus: ReadStatus): List<Book> {
        return bookDao.getBookByStatus(readStatus.name).map {
            it.toBook()
        }
    }

    override suspend fun getAllBooks(): List<Book> {
        return bookDao.getAllBooks().map {
            it.toBook()
        }
    }

    override suspend fun getAllBooksAPI(): List<Book> {
        withContext(Dispatchers.IO) {
            val books = bookApi.getAllBooks()
        }
        return emptyList()
    }
}