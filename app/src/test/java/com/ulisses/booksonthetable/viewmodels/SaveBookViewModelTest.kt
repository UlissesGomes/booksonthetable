package com.ulisses.booksonthetable.viewmodels

import com.ulisses.booksonthetable.domain.interfaces.BookRepository
import com.ulisses.booksonthetable.domain.models.Book
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import com.ulisses.booksonthetable.utils.CoroutineTestRule
import com.ulisses.booksonthetable.utils.LiveDataTestUtil
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class SaveBookViewModelTest {

    @JvmField
    @RegisterExtension
    val coroutinesTestExtension = CoroutineTestRule()

    @RelaxedMockK
    lateinit var repository: BookRepository

    private lateinit var viewModel: SaveBookViewModel

    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
        this.viewModel = SaveBookViewModel(repository)
    }

    @AfterEach
    fun tearDown() {
        unmockkAll()
    }

    @Test
    @DisplayName("Quando livro passado tem readstatus igual a PARA LER, dateBegin e dateEnd deve ser vazio e NÃO deve ser salvo no banco")
    fun errorSaveBook() = runBlockingTest {

        val book = Book(
            title = "TESTE",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = ReadStatus.TO_READ.status,
            dateBegin = "23/09/1997",
            dateEnd = "23/09/1997"
        )

        viewModel.save(book)

        coVerify(exactly = 0) {
            repository.createBook(book)
        }

    }

    @Test
    @DisplayName("Quando livro passado tem readstatus igual a PARA LER, dateBegin deve ser vazio e NÃO deve ser salvo no banco")
    fun errorSaveBook4() = runBlockingTest {

        val book = Book(
            title = "TESTE",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = ReadStatus.TO_READ.status,
            dateBegin = "23/09/1997",
            dateEnd = ""
        )
        viewModel.save(book)

        coVerify(exactly = 0) {
            repository.createBook(book)
        }

    }

    @Test
    @DisplayName("Quando livro passado tem readstatus igual a PARA LER, dateEnd deve ser vazio e NÃO deve ser salvo no banco")
    fun errorSaveBook5() = runBlockingTest {

        val book = Book(
            title = "TESTE",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = ReadStatus.TO_READ.status,
            dateBegin = "",
            dateEnd = "23/09/1997"
        )
        viewModel.save(book)

        coVerify(exactly = 0) {
            repository.createBook(book)
        }

    }

    @Test
    @DisplayName("Quando livro passado tem readstatus igual a JÁ LIDO, e dateEnd é vazio NÃO deve ser salvo no banco")
    fun errorSaveBook2() = runBlockingTest {

        val book = Book(
            title = "TESTE",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = ReadStatus.ALREADY_READ.status,
            dateBegin = "22/03/1995",
            dateEnd = ""
        )
        viewModel.save(book)

        coVerify(exactly = 0) {
            repository.createBook(book)
        }

    }

    @Test
    @DisplayName("Quando livro passado tem readstatus igual a JÁ LIDO, e dateBegin é vazio NÃO deve ser salvo no banco")
    fun errorSaveBook3() = runBlockingTest {

        val book = Book(
            title = "TESTE",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = ReadStatus.ALREADY_READ.status,
            dateBegin = "",
            dateEnd = "22/03/1995"
        )
        viewModel.save(book)

        coVerify(exactly = 0) {
            repository.createBook(book)
        }

    }


    @Test
    @DisplayName("Quando livro passado tem readstatus igual a JÁ LIDO, e dateBegin e dateEnd é vazio NÃO deve ser salvo no banco")
    fun errorDateIfBookALREADY_READ() = runBlockingTest {

        val book = Book(
            title = "TESTE",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = ReadStatus.ALREADY_READ.status,
            dateBegin = "",
            dateEnd = ""
        )
        viewModel.save(book)

        coVerify(exactly = 0) {
            repository.createBook(book)
        }

    }

    @Test
    @DisplayName("Quando livro passado tem status igual JÁ LIDO não deve chamar repository.createBook(book)")
    fun errorDateIfBookREADING() = runBlockingTest {

        val book = Book(
            title = "TESTE",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = ReadStatus.READING.status,
            dateBegin = "",
            dateEnd = ""
        )
        viewModel.save(book)

        coVerify(exactly = 0) {
            repository.createBook(book)
        }

    }

    @Test
    @DisplayName("Quando livro passado está com titulo vazio")
    fun titleIsEmptyTest() {
        val book = Book(
            title = "",
            authorName = "teste",
            genre = "ROMANCE",
            readStatus = "LENDO",
            dateBegin = "12/12/2012",
            dateEnd = "12/12/2012"
        )
        viewModel.save(book)

        Assertions.assertNotNull(LiveDataTestUtil.getLiveDataValue(viewModel.handleErrorTitle()))

    }

    @Test
    @DisplayName("Quando status passado é LENDO mostra campo de input de data")
    fun ifShowDateBegin() {
        viewModel.showDate(ReadStatus.READING.status)
        Assertions.assertEquals(true, viewModel.handleShowDateBegin().value)
    }


}