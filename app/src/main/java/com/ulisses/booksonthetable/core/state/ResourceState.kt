package com.ulisses.booksonthetable.core.state

sealed class ResourceState<out T : Any> {
    data class Success<out T: Any>(val data: T) : ResourceState<T>()
    data class Error(val error: String) : ResourceState<Nothing>()
    object Loading : ResourceState<Nothing>()
}
