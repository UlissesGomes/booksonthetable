package com.ulisses.booksonthetable.core.database.converters

import androidx.room.TypeConverter
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import java.util.*

class Converters {
    @TypeConverter
    fun toReadStatus(value: String) = enumValueOf<ReadStatus>(value)

    @TypeConverter
    fun fromReadStatus(value: ReadStatus) = value.name

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}