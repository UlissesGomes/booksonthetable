package com.ulisses.booksonthetable.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.ulisses.booksonthetable.core.database.converters.Converters
import com.ulisses.booksonthetable.core.database.dao.BookDao
import com.ulisses.booksonthetable.core.database.entities.BookEntity
import com.ulisses.booksonthetable.domain.models.User

@Database(entities = [BookEntity::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun bookDao(): BookDao
}