package com.ulisses.booksonthetable.domain.interfaces

import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.domain.models.User
import com.ulisses.booksonthetable.infrastructure.network.models.UserResponse

interface UserRepository {
    suspend fun register(user: User)
    suspend fun getCredentials(): String
    suspend fun doLogin(user: String, password: String): ResourceState<UserResponse>
}