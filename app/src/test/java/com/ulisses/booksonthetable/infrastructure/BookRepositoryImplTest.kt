package com.ulisses.booksonthetable.infrastructure

import com.ulisses.booksonthetable.core.database.dao.BookDao
import com.ulisses.booksonthetable.core.database.entities.BookEntity
import com.ulisses.booksonthetable.domain.formatters.toFormatString
import com.ulisses.booksonthetable.domain.interfaces.BookRepository
import com.ulisses.booksonthetable.domain.models.Book
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import com.ulisses.booksonthetable.infrastructure.network.interfaces.BookApi
import com.ulisses.booksonthetable.infrastructure.repository.BookRepositoryImpl
import com.ulisses.booksonthetable.utils.CoroutineTestRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension
import java.util.*

@ObsoleteCoroutinesApi
@ExperimentalCoroutinesApi
@ExtendWith(MockKExtension::class)
class BookRepositoryImplTest {

    @JvmField
    @RegisterExtension
    val coroutinesTestExtension = CoroutineTestRule()

    @RelaxedMockK
    lateinit var bookDao: BookDao

    @RelaxedMockK
    lateinit var bookApi: BookApi

    private lateinit var repository: BookRepository

    @BeforeEach
    fun setUp() {
        MockKAnnotations.init(this)
        this.repository =
            BookRepositoryImpl(
                bookDao,
                bookApi
            )
    }

    @Test
    fun test() = runBlockingTest {

        coEvery {
            bookDao.getAllBooks()
        } returns listOf(
            BookEntity(
                id = 1,
                title = "Teste",
                authorName = "Teste",
                dateBegin =  Date(),
                dateEnd = Date(),
                genre = "ROMANCE",
                readStatus = ReadStatus.ALREADY_READ,
                read = true
            )
        )

        val actual = repository.getAllBooks()
        val expected = listOf(
            Book(
                id = 1,
                title = "Teste",
                authorName = "Teste",
                dateBegin =  Date().toFormatString(),
                dateEnd = Date().toFormatString(),
                genre = "ROMANCE",
                readStatus = ReadStatus.ALREADY_READ.status,
                read = true
            )
        )

        Assertions.assertEquals(expected.size, actual.size)
        Assertions.assertEquals(expected[0].readStatus, actual[0].readStatus)
        Assertions.assertEquals(expected[0].title, actual[0].title)

    }

}