package com.ulisses.booksonthetable.ui.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import com.ulisses.booksonthetable.R
import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.domain.models.Category
import com.ulisses.booksonthetable.ui.adapter.BookListHorizontalAdapter
import com.ulisses.booksonthetable.ui.adapter.BookListVerticalAdapter
import com.ulisses.booksonthetable.ui.detail.DetailBookActivity
import com.ulisses.booksonthetable.ui.save.SaveBookActivity
import com.ulisses.booksonthetable.viewmodels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), BookListHorizontalAdapter.ClickListener {

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initObservables()

        btn_save.setOnClickListener {
            startActivity(Intent(this, SaveBookActivity::class.java))
        }

    }

    override fun onResume() {
        super.onResume()
        viewModel.getCategories()
    }

    private fun initObservables() {
        viewModel.handleCategories().observe(this, Observer { resource ->
            when (resource) {
                is ResourceState.Success -> handleSuccess(resource.data)
                is ResourceState.Error -> handleError(resource.error)
                is ResourceState.Loading -> showLoading()
            }
        })
    }

    private fun handleSuccess(categories: List<Category>?) {
        recycle_main.adapter = BookListVerticalAdapter(categories ?: listOf(), this)
        recycle_main.adapter?.notifyDataSetChanged()
    }

    private fun showLoading(){

    }

    private fun handleError(error: String){
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }


    override fun onClick(id: Long) {
        val intent = Intent(this, DetailBookActivity::class.java)
        intent.putExtra("book_id", id)
        startActivity(intent)
    }

}