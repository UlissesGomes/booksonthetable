package com.ulisses.booksonthetable.domain.interfaces

import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import com.ulisses.booksonthetable.domain.models.Book

interface BookRepository {

    suspend fun createBook(book: Book)

    suspend fun deleteBook(book: Book)

    suspend fun getBook(id: Long): Book

    suspend fun getBookByStatus(readStatus: ReadStatus): List<Book>

    suspend fun getAllBooks(): List<Book>

    suspend fun getAllBooksAPI(): List<Book>

}