package com.ulisses.booksonthetable.core.database.dao

import androidx.room.*
import com.ulisses.booksonthetable.core.database.entities.BookEntity

@Dao
interface BookDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(book: BookEntity)

    @Delete
    suspend fun delete(book: BookEntity)

    @Query("SELECT * FROM BookEntity WHERE ID = :id")
    suspend fun getBook(id: Long): BookEntity

    @Query("SELECT * FROM BookEntity")
    suspend fun getAllBooks(): List<BookEntity>

    @Query("SELECT * FROM BookEntity where readStatus = :status")
    suspend fun getBookByStatus(status: String): List<BookEntity>



}