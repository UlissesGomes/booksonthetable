package com.ulisses.booksonthetable.domain.models

import java.io.Serializable

data class Book(
    val id: Long = 0,
    val title: String,
    val authorName: String,
    val genre: String,
    val readStatus: String,
    val read: Boolean = false,
    val dateBegin: String,
    val dateEnd: String
) : Serializable