package com.ulisses.booksonthetable.domain.models

class User(val id: Long, val fullName: String, val email: String, val credentials: String)