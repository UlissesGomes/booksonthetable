package com.ulisses.booksonthetable.infrastructure.network.interfaces

import com.ulisses.booksonthetable.infrastructure.network.models.BookResponse
import retrofit2.http.GET

interface BookApi {
    @GET("books")
    suspend fun getAllBooks(): List<BookResponse>
}