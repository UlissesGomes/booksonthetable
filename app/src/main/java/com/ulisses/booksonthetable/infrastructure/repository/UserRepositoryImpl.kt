package com.ulisses.booksonthetable.infrastructure.repository

import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.domain.interfaces.UserRepository
import com.ulisses.booksonthetable.domain.models.User
import com.ulisses.booksonthetable.infrastructure.network.interfaces.LoginApi
import com.ulisses.booksonthetable.infrastructure.network.models.UserResponse

class UserRepositoryImpl(private val loginApi: LoginApi): UserRepository {

    override suspend fun doLogin(user: String, password: String): ResourceState<UserResponse> {
        val userResponse = loginApi.login(user, password)

        if (userResponse.isSuccessful) {
            val user = userResponse.body()
            user?.let {
                return ResourceState.Success(it)
            }
        }

        if (userResponse.code() == 401) {
            return ResourceState.Error("Usuário ou senha incorreto")
        }

        return ResourceState.Error("Erro ao tentar fazer login")
    }

    override suspend fun register(user: User) {
        TODO("Not yet implemented")
    }

    override suspend fun getCredentials(): String {
        TODO("Not yet implemented")
    }

}