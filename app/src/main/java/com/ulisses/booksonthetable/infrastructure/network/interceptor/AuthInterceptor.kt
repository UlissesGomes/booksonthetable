package com.ulisses.booksonthetable.infrastructure.network.interceptor

import okhttp3.Credentials
import okhttp3.Interceptor

class AuthInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val credentials = Credentials.basic("", "")
        val request = chain
            .request()
            .newBuilder()
            .header("Authorization", credentials)
            .build()
        return chain.proceed(request)
    }
}