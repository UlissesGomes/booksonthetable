package com.ulisses.booksonthetable.ui.login

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.ulisses.booksonthetable.R
import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.infrastructure.network.models.UserResponse
import com.ulisses.booksonthetable.viewmodels.LoginViewModel
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        spannable()

        btn_login.setOnClickListener {
            loginViewModel.doLogin(edit_email.text.toString(), edit_password.text.toString())
        }
    }

    override fun onStart() {
        super.onStart()
        initObservables()
    }

    private fun initObservables() {
        loginViewModel.handleLogin().observe(this, Observer { resource ->
            when(resource) {
                is ResourceState.Success -> handleSuccess(resource.data)
                is ResourceState.Error -> handleError(resource.error)
                is ResourceState.Loading -> showLoading()
            }
        })

        loginViewModel.handleError().observe(this, Observer {
            Toast.makeText(this,"Preencha os campos", Toast.LENGTH_SHORT).show()
        })

    }

    private fun handleSuccess(user: UserResponse) {
        Toast.makeText(this, user.fullName, Toast.LENGTH_SHORT).show()
    }
    private fun handleError(error: String){
        Toast.makeText(this,error, Toast.LENGTH_SHORT).show()
    }

    private fun showLoading(){

    }

    private fun spannable() {
        val txtValueRegister = txt_sign_up.text.toString()
        val spannable = SpannableString(txtValueRegister)
        spannable.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.purple_200)),
            15, 26,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        txt_sign_up.text = spannable
    }
}