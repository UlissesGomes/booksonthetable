package com.ulisses.booksonthetable.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ulisses.booksonthetable.core.bases.BaseViewModel
import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.domain.interfaces.UserRepository
import com.ulisses.booksonthetable.infrastructure.network.models.UserResponse
import kotlinx.coroutines.launch

class LoginViewModel(private val repository: UserRepository): BaseViewModel() {

    private val _doLogin = MutableLiveData<ResourceState<UserResponse>>()
    private val _errorEmptyField = MutableLiveData<Boolean>()

    fun handleLogin(): LiveData<ResourceState<UserResponse>> = _doLogin
    fun handleError(): LiveData<Boolean> = _errorEmptyField

    fun doLogin(email: String, password: String) {
        launch {

            if (email.isNotEmpty() || password.isNotEmpty()) {
                _doLogin.value = repository.doLogin(email, password)
            }else {
                _errorEmptyField.value = true
            }

        }


    }

}