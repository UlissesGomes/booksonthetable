package com.ulisses.booksonthetable.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ulisses.booksonthetable.core.bases.BaseViewModel
import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.domain.interfaces.BookRepository
import com.ulisses.booksonthetable.domain.models.Category
import com.ulisses.booksonthetable.domain.models.enums.ReadStatus
import kotlinx.coroutines.launch

class MainViewModel(private val repository: BookRepository): BaseViewModel() {

    private val _listCategories = MutableLiveData<ResourceState<List<Category>>>()

    fun handleCategories(): LiveData<ResourceState<List<Category>>> = _listCategories

    fun getCategories() {
        launch {
            val booksToReady = repository.getBookByStatus(ReadStatus.TO_READ)
            val booksAlreadyRead = repository.getBookByStatus(ReadStatus.ALREADY_READ)
            val booksReading = repository.getBookByStatus(ReadStatus.READING)

            val categories = listOf(
                Category(
                    title = ReadStatus.TO_READ.status,
                    books = booksToReady
                ),
                Category(
                    title = ReadStatus.ALREADY_READ.status,
                    books = booksAlreadyRead
                ),
                Category(
                    title = ReadStatus.READING.status,
                    books = booksReading
                )
            )

            _listCategories.value = ResourceState.Success(categories)

        }
    }
}