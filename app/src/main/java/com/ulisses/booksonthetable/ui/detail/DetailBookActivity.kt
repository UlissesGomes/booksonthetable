package com.ulisses.booksonthetable.ui.detail

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.ulisses.booksonthetable.R
import com.ulisses.booksonthetable.core.state.ResourceState
import com.ulisses.booksonthetable.domain.models.Book
import com.ulisses.booksonthetable.ui.save.SaveBookActivity
import com.ulisses.booksonthetable.viewmodels.DetailBookViewModel
import kotlinx.android.synthetic.main.activity_detail_book.*
import kotlinx.android.synthetic.main.top_bar_no_icon.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailBookActivity : AppCompatActivity() {

    private val viewModel: DetailBookViewModel by viewModel()

    var id: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_book)
        card_view.setBackgroundResource(R.drawable.shape_card_view)

        initObservables()

        id = intent.getLongExtra("book_id", 0)

    }

    override fun onResume() {
        super.onResume()
        viewModel.getBook(id)
    }


    private fun initObservables() {
        viewModel.handleBook().observe(this, Observer { resource ->
            when (resource) {
                is ResourceState.Success -> handleSuccess(resource.data)
                is ResourceState.Error -> handleError(resource.error)
                is ResourceState.Loading -> showLoading()
            }
        })
        viewModel.handleHideDateBegin().observe(this, Observer { resource ->
            title_date_begin.visibility =  if (resource) View.GONE else View.VISIBLE
            txt_date_begin.visibility = if (resource) View.GONE else View.VISIBLE
        })
        viewModel.handleHideDateEnd().observe(this, Observer { resource ->
            title_date_end.visibility = if (resource) View.GONE else View.VISIBLE
            txt_date_end.visibility = if (resource) View.GONE else View.VISIBLE
        })
        viewModel.handleHideButtonRead().observe(this, Observer { resource ->
            btn_mark_as_read.visibility = if (resource) View.GONE else View.VISIBLE
        })
        viewModel.handleDeleteBook().observe(this, Observer {
            finish()
        })
    }

    private fun handleSuccess(book: Book) {
        fill(book)
        actions(book)
    }

    private fun handleError(error: String){
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    private fun showLoading() {
        //s[inner
    }

    private fun fill(book: Book) {
        include.top_bar_no_icon_title.text = book.title
        txt_author.text = book.authorName
        txt_genre.text = book.genre
        txt_status.text = book.readStatus
        txt_date_begin.text = book.dateBegin
        txt_date_end.text = book.dateEnd
    }

    private fun actions(book: Book) {
        btn_edit_book.setOnClickListener {
            val intent = Intent(this, SaveBookActivity::class.java)
            intent.putExtra("book", book)
            startActivity(intent)
        }

        btn_delete_book.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder
                .setMessage("Deseja deletar esse livro?")
                .setPositiveButton("Sim") { _, _ ->
                    viewModel.delete(book)
                }
                .setNegativeButton("Não") { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }
    }


}