package com.ulisses.booksonthetable.infrastructure.network.interfaces

import com.ulisses.booksonthetable.infrastructure.network.models.UserResponse
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginApi {

    @FormUrlEncoded
    @POST("login")
    suspend fun login(@Field("username") email: String, @Field("password") password: String): Response<UserResponse>

}