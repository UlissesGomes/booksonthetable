package com.ulisses.booksonthetable.core.di

import android.app.Application
import androidx.room.Room
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.ulisses.booksonthetable.core.bases.BASE_URL
import com.ulisses.booksonthetable.core.database.AppDatabase
import com.ulisses.booksonthetable.core.database.dao.BookDao
import com.ulisses.booksonthetable.domain.interfaces.BookRepository
import com.ulisses.booksonthetable.domain.interfaces.UserRepository
import com.ulisses.booksonthetable.infrastructure.network.interceptor.AuthInterceptor
import com.ulisses.booksonthetable.infrastructure.network.interfaces.BookApi
import com.ulisses.booksonthetable.infrastructure.network.interfaces.LoginApi
import com.ulisses.booksonthetable.infrastructure.repository.BookRepositoryImpl
import com.ulisses.booksonthetable.infrastructure.repository.UserRepositoryImpl
import com.ulisses.booksonthetable.viewmodels.DetailBookViewModel
import com.ulisses.booksonthetable.viewmodels.LoginViewModel
import com.ulisses.booksonthetable.viewmodels.MainViewModel
import com.ulisses.booksonthetable.viewmodels.SaveBookViewModel
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val databaseModule = module {

    fun provideDatabase(application: Application): AppDatabase {
        return Room.databaseBuilder(
            application,
            AppDatabase::class.java,
            "app.db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    fun provideDao(database: AppDatabase): BookDao {
        return database.bookDao()
    }

    single<AppDatabase> {
        provideDatabase(androidApplication())
    }

    single {
        provideDao(get())
    }

}

val viewModelModule = module {
    viewModel {
        SaveBookViewModel(repository = get())
    }

    viewModel {
        MainViewModel(repository = get())
    }

    viewModel {
        DetailBookViewModel(repository = get())
    }
    viewModel {
        LoginViewModel(repository = get())
    }
}


val netModule = module {

    fun provideHttpClient(authInterceptor: AuthInterceptor): OkHttpClient {
        val okHttpClientBuilder = OkHttpClient.Builder()
        okHttpClientBuilder.addInterceptor(authInterceptor)
        return okHttpClientBuilder.build()
    }

    fun provideGson(): Gson {
        return GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create()
    }

    fun provideRetrofit(factory: Gson, client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(factory))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(client)
            .build()

    }

    single { provideHttpClient(get()) }
    single { provideGson() }
    single { provideRetrofit(get(), get()) }
    factory { AuthInterceptor() }

}

val apiModule = module {
    fun provideBookApi(retrofit: Retrofit): BookApi {
        return retrofit.create(BookApi::class.java)
    }

    fun provideLoginApi(retrofit: Retrofit): LoginApi {
        return retrofit.create(LoginApi::class.java)
    }
    single { provideBookApi(get()) }
    single { provideLoginApi(get()) }
}


val repositoryModule = module {

    fun provideBookRepository(dao: BookDao, bookApi: BookApi): BookRepository {
        return BookRepositoryImpl(
            bookDao = dao,
            bookApi = bookApi
        )
    }

    fun provideLoginRepository(loginApi: LoginApi): UserRepository {
        return UserRepositoryImpl(loginApi)
    }

    single {
        provideBookRepository(get(), get())
    }
    single {
        provideLoginRepository(get())
    }

}

