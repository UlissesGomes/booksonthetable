package com.ulisses.booksonthetable.domain.models.enums

enum class ReadStatus(val status: String) {
    READING("LENDO"), ALREADY_READ("JÁ LIDO"), TO_READ("PARA LER");

    companion object {
        private val map = values().associateBy(ReadStatus::status)
        fun fromString(status: String) = map[status]
    }
}
